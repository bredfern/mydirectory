var dealerData = [
	{					
		dealerid: "acu-scv-1",
		makeid: "acu",
		make: "Acura",
		dealer: "Valencia Acura",
		area: "SCV",
		address: "23955 Creekside Road Valencia, CA 91335",
		phone: "3109290492",
		image: "acura.jpg"	
	},				
	{
		dealerid: "acu-gld-1",
		makeid: "acu",
		make: "Acura",
		dealer: "Acura of Glendale",
		area: "GLD",
		address: "South Brand Blvd Glendale 91204",
		phone: "3109290492",
		image: "acura.jpg"	
	},
	{
		dealerid: "acu-wst-1",
		makeid: "acu",
		make: "Acura",
		dealer: "Santa Monica Acura",
		area: "WLA",
		address: "1717 Santa Monica Blvd Santa Monica 90404",
		phone: "3109290492",
		image: "acura.jpg"	
	},
	{
		dealerid: "acu-sby-1",
		makeid: "acu",
		make: "Acura",
		dealer: "Cerritos Acura",
		area: "SBY",
		address: "18827 Studebaker Road Cerritos 90703",
		phone: "3109290492",
		image: "acura.jpg"	
	},
	{
		dealerid: "acu-sby-2",
		makeid: "acu",
		make: "Acura",
		dealer: "Power Acura of Southbay",
		area: "SBY",
		address: "25341 Crenshaw Boulevard Torrance 90505",
		phone: "3109290492",
		image: "acura.jpg"	
	},
	{
		dealerid: "ast-sfv-1",
		makeid: "ast",
		make: "Aston Martin",
		dealer: "Galpin Aston Martin",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",
		phone: "3109290492",
		image: "astonmartin.jpg"	
	},
	{
		dealerid: "aud-sfv-1",
		makeid: "aud",
		make: "Audi",
		dealer: "Keyes Audi",
		area: "SFV",
		address: "5239 Van Nuys Boulevard Van Nuys 91401",
		phone: "3109290492",
		image: "audi.jpg"	
	},
	{
		dealerid: "aud-mid-1",
		makeid: "aud",
		make: "Audi",
		dealer: "Downtown L.A. Motors - Porche/Audi/Volkswagen",
		area: "MID",
		address: "1900 S. Figueroa St. Los Angeles 90007",
		phone: "3109290492",
		image: "audi.jpg"	
	},
	{
		dealerid: "aud-sby-1",
		makeid: "aud",
		make: "Audi",
		dealer: "Circle Imports Audi, Porche",
		area: "SBY",
		address: "1919 North Lakewood Boulevard Long Beach 90815",
		phone: "3109290492",
		image: "audi.jpg"	
	},
	{
		dealerid: "aud-sby-2",
		makeid: "aud",
		make: "Audi",
		dealer: "Pacific Audi",
		area: "SBY",
		address: "1919 North Lakewood Boulevard Long Beach 90815",
		phone: "3109290492",
		image: "audi.jpg"	
	},
	{
		dealerid: "ben-mid-1",
		makeid: "ben",
		make: "Bentley",
		dealer: "O'Gara Coach Company",
		area: "MID",
		address: "8833 West Olympic Blvd Beverly Hills,CA,90211",
		phone: "3109290492",
		image: "bentley.jpg"
	},
	{
		dealerid: "bmw-sfv-1",
		makeid: "bmw",
		make: "BMW",
		dealer: "Center BMW",
		area: "SFV",
		address: "5201 Van Nuys Blvd Sherman Oaks CA ,91401",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-sfv-2",
		makeid: "bmw",
		make: "BMW",
		dealer: "Bob Smith BMW",
		area: "SFV",
		address: "24500 Calabasas Road Calabasas CA,91302",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-sfv-3",
		makeid: "bmw",
		make: "BMW",
		dealer: "Century West BMW",
		area: "SFV",
		address: "4245 Lankershim Blvd North Hollywood CA, 91602",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-sgv-1",
		makeid: "bmw",
		make: "BMW",
		dealer: "New Century BMW",
		area: "SGV",
		address: "1139 West Main Street Alhambra 91801",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-mid-1",
		makeid: "bmw",
		make: "BMW",
		dealer: "Nick Alexander Imports BMW",
		area: "MID",
		address: "6333 S. Alameda Street Los Angeles 90001",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-mid-2",
		makeid: "bmw",
		make: "BMW",
		dealer: "Beveryly Hills BMW",
		area: "MID",
		address: "5070 Wilshire Boulevard Los Angeles 90036",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bmw-sby-1",
		makeid: "bmw",
		make: "BMW",
		dealer: "Southbay BMW",
		area: "SBY",
		address: "18800 Hawthorne Boulevard Torrance 90504",
		phone: "3109290492",
		image: "bmw.jpg"
	},
	{
		dealerid: "bug-mid-1",
		makeid: "bug",
		make: "Bugatti",
		dealer: "O'Gara Coach Company",
		area: "MID",
		address: "8833 West Olympic Blvd Beverly Hills,CA,90211",
		phone: "3109290492",
		image: "bugatti.jpg"
	},
	{
		dealerid: "bui-sgv-1",
		makeid: "bui",
		make: "Buik",
		dealer: "O'DONNELL CHEVROLET/BUICK",
		area: "SGV",
		address: "100 South San Gabriel Boulevard San Gabriel 91776",	
		phone: "3109290492",
		image: "buick.jpg"
	},
	{
		dealerid: "bui-sby-1",
		makeid: "bui",
		make: "Buik",
		dealer: "BOULEVARD BUICK/PONTIAC/GMC",
		area: "SBY",
		address: "2800 Cherry Avenue Long Beach 90755",	
		phone: "3109290492",
		image: "buick.jpg"
	},
	{
		dealerid: "cad-sgv-1",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Symes Cadillac",
		area: "SGV",
		address: "3475 E. Colorado Blvd Pasadena 91107",
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "cad-sgv-2",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Bewley Allen Cadillac",
		area: "SGV",
		address: "801 East Main St Alhambra 91801",	
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "cad-sgv-3",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Crestview Cadillac",
		area: "SGV",
		address: "2700 E Garvey Avenue South West Covina 91791",
		phone: "3109290492",
		image: "cadillac.jpg"
	},	
	{
		dealerid: "cad-mid-1",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Felix Cadillac/Chevrolet",
		area: "MID",
		address: "635 W. Washington Blvd Los Angeles 90015",
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "cad-wst-1",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Martin Cadillac/Pontiac",
		area: "WST",
		address: "12101 West Olympic Blvd Los Angeles 90064",	
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "cad-sby-1",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Penske Cadillac Hummer South Bay",
		area: "SBY",
		address: "18600 Hawthorne Boulevard Torrance 90504",
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "cad-sby-2",
		makeid: "cad",
		make: "Cadillac",
		dealer: "Coast Cadillac",
		area: "SBY",
		address: "3399 E. Willow Avenue Long Beach 90806",
		phone: "3109290492",
		image: "cadillac.jpg"
	},
	{
		dealerid: "che-scv-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Power Chevrolet Valencia",
		area: "SCV",
		address: "23649 Valencia Boulevard Valencia 91335",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sfv-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Keyes Chevrolet",
		area: "SFV",
		address: "6001 Van Nuys Boulevard Van Nuys 91401",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-gld-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Allen Gwynn Chevrolet",
		area: "GLD",
		address: "1400 South Brand Blvd Glendale 91204",
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-gld-2",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Community Chevrolet",
		area: "GLD",
		address: "200 West Olive Avenue Burbank 91502",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sgv-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Allen Gwynn Chevrolet",
		area: "SGV",
		address: "100 South San Gabriel Blvd San Gabriel 91776",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sgv-2",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Puente Hills Chevrolet",
		area: "SGV",
		address: "17300 E. Gale Avenue City of Industry 91748",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sgv-3",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Sierra Chevrolet",
		area: "SGV",
		address: "721 E Central Ave. Monrovia 91016",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-mid-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Felix Cadillac/Chevrolet",
		area: "MID",
		address: "635 W. Washington Blvd Los Angeles 90015",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sby-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "George Chevrolet",
		area: "SBY",
		address: "17000 Lakewood Boulevard Bellflower 90706",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-sby-2",
		makeid: "che",
		make: "Chevrolet",
		dealer: "John Giacomin Chevrolet",
		area: "SBY",
		address: "23505 Hawthorne Blvd Torrance 90505",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "che-av-1",
		makeid: "che",
		make: "Chevrolet",
		dealer: "Rally Automotive Group Chevrolet",
		area: "AV",
		address: "39012 Carriage Way Palmdale 93551",	
		phone: "3109290492",
		image: "chevy.jpg"
	},
	{
		dealerid: "chr-scv-1",
		makeid: "chr",
		make: "Chrysler",
		dealer: "Valencia Dodge, Jeep, Chrysler",
		area: "SCV",
		address: "23820 Creekside Rd Valencia 91335",	
		phone: "3109290492",
		image: "chrysler.jpg"
	},
	{
		dealerid: "chr-gld-1",
		makeid: "chr",
		make: "Chrysler",
		dealer: "Glendale Dodge, Jeep, Chrysler",
		area: "GLD",
		address: "1101 South Brand Boulevard Glendale 91204",	
		phone: "3109290492",
		image: "chrysler.jpg"
	},
	{
		dealerid: "chr-mid-1",
		makeid: "chr",
		make: "Chrysler",
		dealer: "Burge Chrysler/Jeep",
		area: "MID",
		address: "11750 Santa Monica Blvd Los Angeles 90025",	
		phone: "3109290492",
		image: "chrysler.jpg"
	},
	{
		dealerid: "chr-sby-1",
		makeid: "chr",
		make: "Chrysler",
		dealer: "Southbay Chrysler Jeep Dodge",
		area: "SBY",
		address: "20900 Hawthorne Boulevard Torrance 90503",
		phone: "3109290492",
		image: "chrysler.jpg"
	},
	{
		dealerid: "dod-scv-1",
		makeid: "dod",
		make: "Dodge",
		dealer: "Valencia Dodge, Jeep, Chrysler",
		area: "SCV",
		address: "23820 Creekside Rd Valencia 91335",	
		phone: "3109290492",
		image: "dodge.jpg"
	},
	{
		dealerid: "dod-gld-1",
		makeid: "dod",
		make: "Dodge",
		dealer: "Glendale Dodge, Jeep, Chrysler",
		area: "GLD",
		address: "1101 South Brand Boulevard Glendale 91204",	
		phone: "3109290492",
		image: "dodge.jpg"
	},
	{
		dealerid: "dod-mid-1",
		makeid: "dod",
		make: "Dodge",
		dealer: "Burge Chrysler/Jeep",
		area: "MID",
		address: "11750 Santa Monica Blvd Los Angeles 90025",	
		phone: "3109290492",
		image: "dodge.jpg"
	},
	{
		dealerid: "dod-sby-1",
		makeid: "dod",
		make: "Dodge",
		dealer: "Southbay Chrysler Jeep Dodge",
		area: "SBY",
		address: "20900 Hawthorne Boulevard Torrance 90503",
		phone: "3109290492",
		image: "dodge.jpg"
	},
	{
		dealerid: "for-scv-1",
		makeid: "for",
		make: "Ford",
		dealer: "Power Ford Valencia",
		area: "SCV",
		address: "23920 Creekside Rd Valencia, CA 91335",
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sfv-1",
		makeid: "for",
		make: "Ford",
		dealer: "Galpin Motors (Ford)",
		area: "SFV",
		address: "15505 Roscoe Blvd North Hills 91343",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sfv-2",
		makeid: "for",
		make: "Ford",
		dealer: "Sunrise Ford",
		area: "SFV",
		address: "5500 Lankershim Blvd North Hollywood 91601",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sfv-3",
		makeid: "for",
		make: "Ford",
		dealer: "Vista Ford",
		area: "SFV",
		address: "21501 Ventura Blvd Woodland Hills 91364",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sgv-1",
		makeid: "for",
		make: "Ford",
		dealer: "Colley Auto Cars Ford",
		area: "SGV",
		address: "1945 Auto Centre Drive Glendora 91740",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sgv-2",
		makeid: "for",
		make: "Ford",
		dealer: "Bob Wondries Ford",
		area: "SGV",
		address: "400 South Atlantic Boulevard Alhambra 91801",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sgv-3",
		makeid: "for",
		make: "Ford",
		dealer: "Advantage Ford",
		area: "SGV",
		address: "1051 Central Ave Duarte 91010",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sgv-4",
		makeid: "for",
		make: "Ford",
		dealer: "Ford of Montebello",
		area: "SGV",
		address: "2747 Via Campo Montebello 90640",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sgv-5",
		makeid: "for",
		make: "Ford",
		dealer: "Ed Butts Ford",
		area: "SGV",
		address: "1515 North Hacienda  Blvd La Puente 91744",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-mid-1",
		makeid: "for",
		make: "Ford",
		dealer: "Buerge Ford",
		area: "MID",
		address: "11800 Santa Monica Blvd Los Angeles 90025",
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-wst-1",
		makeid: "for",
		make: "Ford",
		dealer: "Airport Marina Ford",
		area: "WST",
		address: "5880 W Centinela Ave Los Angeles 90045",
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-wst-2",
		makeid: "for",
		make: "Ford",
		dealer: "Santa Monica Ford",
		area: "WST",
		address: "1230 Santa Monica Blvd Santa Monica 90404",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sby-1",
		makeid: "for",
		make: "Ford",
		dealer: "CERRITOS FORD LINCOLN MERCURY HYUNDAI",
		area: "SBY",
		address: "18900 Studebaker Road Cerritos 90703",
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sby-2",
		makeid: "for",
		make: "Ford",
		dealer: "POWER FORD TORRANCE",
		area: "SBY",
		address: "3311 Pacific Coast Hwy Torrance 90505",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sby-3",
		makeid: "for",
		make: "Ford",
		dealer: "SOUTH BAY FORD LINCOLN MERCURY",
		area: "SBY",
		address: "5100 West Rosecrans Ave Hawthorne 90250",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sby-4",
		makeid: "for",
		make: "Ford",
		dealer: "PACIFIC FORD/LINCOLN-MERCURY",
		area: "SBY",
		address: "3600 Cherry Avenue Long Beach 90807",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-sby-5",
		makeid: "for",
		make: "Ford",
		dealer: "Worthington Ford",
		area: "SBY",
		address: "2950 Bellflower Blvd Long Beach 90815",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "for-av-1",
		makeid: "for",
		make: "Ford",
		dealer: "Antelope Valley Ford",
		area: "AV",
		address: "1155 Auto Mall Dr Lancaster,CA,93534",	
		phone: "3109290492",
		image: "ford.jpg"
	},
	{
		dealerid: "gmc-sgv-1",
		makeid: "gmc",
		make: "GMC",
		dealer: "Reynolds Buick GMC Trucks",
		area: "SGV",
		address: "345 N. Citrus Ave West Covina 91791",	
		phone: "3109290492",
		image: "gmc.jpg"
	},
	{
		dealerid: "gmc-sby-1",
		makeid: "gmc",
		make: "GMC",
		dealer: "Boulevard Buick/Pontiac/GMC",
		area: "SBY",
		address: "2800 Cherry Avenue Long Beach 90755",	
		phone: "3109290492",
		image: "gmc.jpg"
	},
	{
		dealerid: "hon-scv-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Power Honda Valencia",
		area: "SCV",
		address: "23551 Magic Mountain Pkwy Santa Clarita, CA 91335",
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sfv-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Galpin Honda",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sfv-2",
		makeid: "hon",
		make: "Honda",
		dealer: "Keyes Woodland Hills Honda",
		area: "SFV",
		address: "6111 Topanga Canyon Boulevard Woodland Hills 91367",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sfv-3",
		makeid: "hon",
		make: "Honda",
		dealer: "Robertson Honda",
		area: "SFV",
		address: "5841 Lankershim Boulevard North Hollywood 91601",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sgv-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Honda of Pasadena",
		area: "SGV",
		address: "1965 E. Foothill Blvd. Pasadena 91107",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sgv-2",
		makeid: "hon",
		make: "Honda",
		dealer: "Sierra Honda",
		area: "SGV",
		address: "1450 South Shamrock Ave. Monrovia 91016",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sgv-3",
		makeid: "hon",
		make: "Honda",
		dealer: "Norm Reeves Honda Superstore (W. Covina)",
		area: "SGV",
		address: "1840 East Garvey Ave. South West Covina 91791",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sgv-4",
		makeid: "hon",
		make: "Honda",
		dealer: "Nelson Honda",
		area: "SGV",
		address: "3464 North Peck Rd El Monte 91731",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-mid-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Honda of Hollywood",
		area: "MID",
		address: "6511 Santa Monica Blvd Hollywood 90038",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-wst-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Airport Marina Honda",
		area: "WST",
		address: "5850 W Centinela Avenue Los Angeles 90045",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-wst-2",
		makeid: "hon",
		make: "Honda",
		dealer: "Honda of Santa Monica",
		area: "WST",
		address: "1720 Santa Monica Boulevard Santa Monica 90404",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-wst-3",
		makeid: "hon",
		make: "Honda",
		dealer: "Miller Honda of Culver City",
		area: "WST",
		address: "9055 Washington Blvd Culver City 90232",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sby-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Long Beach Honda",
		area: "SBY",
		address: "1500 East Spring Street Signal Hill 90806",
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sby-2",
		makeid: "hon",
		make: "Honda",
		dealer: "Norm Reeves Honda Superstore (Cerritos)",
		area: "SBY",
		address: "18707 Studebaker Rd Cerritos 90703",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-sby-3",
		makeid: "hon",
		make: "Honda",
		dealer: "Carson Honda",
		area: "SBY",
		address: "1435 East 223rd Street Carson 90745",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hon-av-1",
		makeid: "hon",
		make: "Honda",
		dealer: "Robertson's Palmdale Honda",
		area: "AV",
		address: "455 Auto Vista Drive Palmdale 93551",	
		phone: "3109290492",
		image: "honda.jpg"
	},
	{
		dealerid: "hyu-sfv-1",
		makeid: "hyu",
		make: "Hyundai",
		dealer: "Keyes Hyundai",
		area: "SFV",
		address: "5319 Van Nuys Blvd Van Nuys 91401",	
		phone: "3109290492",
		image: "hyundai.jpg"
	},
	{
		dealerid: "hyu-sby-1",
		makeid: "hyu",
		make: "Hyundai",
		dealer: "Cerritos Ford Lincoln Mercury Hyundai",
		area: "SBY",
		address: "18900 Studebaker Road Cerritos 90703",	
		phone: "3109290492",
		image: "hyundai.jpg"
	},
	{
		dealerid: "inf-gld-1",
		makeid: "inf",
		make: "Infiniti",
		dealer: "Glendale Infiniti",
		area: "GLD",
		address: "812 S. Brand Boulevard Glendale 91204",
		phone: "3109290492",
		image: "infiniti.jpg"
	},
	{
		dealerid: "inf-sgv-1",
		makeid: "inf",
		make: "Infiniti",
		dealer: "Metro Infiniti",
		area: "SGV",
		address: "821 East Central Ave Monrovia 91016",	
		phone: "3109290492",
		image: "infiniti.jpg"
	},
	{
		dealerid: "inf-sby-1",
		makeid: "inf",
		make: "Infiniti",
		dealer: "Infiniti of Southbay",
		area: "SBY",
		address: "3035 Pacific Coast Hwy Torrance 90505",
		phone: "3109290492",
		image: "infiniti.jpg"
	},
	{
		dealerid: "jag-sfv-1",
		makeid: "jag",
		make: "Jaguar",
		dealer: "Galpin Jaguar",
		area: "SFV",
		address: "15500 Roscoe Boulevard Van Nuys, CA 91406",	
		phone: "3109290492",
		image: "jaguar.jpg"
	},
	{
		dealerid: "jee-scv-1",
		makeid: "jee",
		make: "Jeep",
		dealer: "Valencia Dodge, Jeep, Chrysler",
		area: "SCV",
		address: "23820 Creekside Rd Valencia 91335",	
		phone: "3109290492",
		image: "jeep.jpg"
	},
	{
		dealerid: "jee-gld-1",
		makeid: "jee",
		make: "Jeep",
		dealer: "Glendale Dodge, Jeep, Chrysler",
		area: "GLD",
		address: "1101 South Brand Boulevard Glendale 91204",	
		phone: "3109290492",
		image: "jeep.jpg"
	},
	{
		dealerid: "jee-mid-1",
		makeid: "jee",
		make: "Jeep",
		dealer: "Burge Chrysler/Jeep",
		area: "MID",
		address: "11750 Santa Monica Blvd Los Angeles 90025",	
		phone: "3109290492",
		image: "jeep.jpg"
	},
	{
		dealerid: "jee-sby-1",
		makeid: "jee",
		make: "Jeep",
		dealer: "Southbay Chrysler Jeep Dodge",
		area: "SBY",
		address: "20900 Hawthorne Boulevard Torrance 90503",
		phone: "3109290492",
		image: "jeep.jpg"
	},
	{
		dealerid: "lam-mid-1",
		makeid: "lam",
		make: "Lamborghini",
		dealer: "O'Gara Coach Company",
		area: "SBY",
		address: "8833 West Olympic Blvd Beverly Hills,CA,90211",	
		phone: "3109290492",
		image: "lamborghini.jpg"
	},
	{
		dealerid: "lan-sfv-1",
		makeid: "lan",
		make: "Land Rover",
		dealer: "Land Rover Encino",
		area: "SFV",
		address: "15800 Ventura Blvd Encino,ca,91436",	
		phone: "3109290492",
		image: "landrover.jpg"
	},
	{
		dealerid: "lan-sgv-1",
		makeid: "lan",
		make: "Land Rover",
		dealer: "Land Rover of Pasadena",
		area: "SGV",
		address: "3485 E Colorado Blvd, Pasadena 91107",
		phone: "3109290492",
		image: "landrover.jpg"
	},
	{
		dealerid: "lan-sby-1",
		makeid: "lan",
		make: "Land Rover",
		dealer: "Land Rover of South Bay",
		area: "SBY",
		address: "900 N Pacific Coast Hwy Redondo Beach 90277",	
		phone: "3109290492",
		image: "landrover.jpg"
	},
	{
		dealerid: "lex-scv-1",
		makeid: "lex",
		make: "Lexus",
		dealer: "Lexus of Valencia",
		area: "SCV",
		address: "24033 Creekside Rd Valencia, CA 91335",
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-sfv-1",
		makeid: "lex",
		make: "Lexus",
		dealer: "Keyes Lexus",
		area: "SFV",
		address: "5905 Van Nuys Boulevard Van Nuys 91401",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-sfv-2",
		makeid: "lex",
		make: "Lexus",
		dealer: "Vista Lexus",
		area: "SFV",
		address: "21611 Ventura Boulevard, Woodland Hills, CA 91364",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-mid-1",
		makeid: "lex",
		make: "Lexus",
		dealer: "Lexus of Beverly Hills",
		area: "MID",
		address: "9230 Wilshire Boulevard Beverly Hills 90212",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-wst-1",
		makeid: "lex",
		make: "Lexus",
		dealer: "Lexus of Santa Monica",
		area: "WST",
		address: "1501 Santa Monica Boulevard Santa Monica 90404",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-sby-1",
		makeid: "lex",
		make: "Lexus",
		dealer: "Lexus of Cerritos",
		area: "SBY",
		address: "18800 Studebaker Road Cerritos 90703",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lex-sby-2",
		makeid: "lex",
		make: "Lexus",
		dealer: "Longo Toyota/Lexus",
		area: "SBY",
		address: "3530 North Peck Road El Monte 91731",	
		phone: "3109290492",
		image: "lexus.jpg"
	},
	{
		dealerid: "lin-sfv-1",
		makeid: "lin",
		make: "Lincoln",
		dealer: "Vista Lincoln Mercury Motor Co",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",	
		phone: "3109290492",
		image: "lincoln.jpg"
	},
	{
		dealerid: "lin-sfv-2",
		makeid: "lin",
		make: "Lincoln",
		dealer: "Mayberry Lincoln Mercury",
		area: "SFV",
		address: "4437 Lankershim Blvd North Hollywood 91602",	
		phone: "3109290492",
		image: "lincoln.jpg"
	},
	{
		dealerid: "lin-sfv-3",
		makeid: "lin",
		make: "Lincoln",
		dealer: "Galpin Lincoln Mercury",
		area: "SFV",
		address: "15505 Roscoe Blvd North Hills 91343",	
		phone: "3109290492",
		image: "lincoln.jpg"
	},
	{
		dealerid: "lot-sfv-1",
		makeid: "lot",
		make: "Lotus",
		dealer: "Galpin Lotus",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",	
		phone: "3109290492",
		image: "lotus.jpg"
	},
	{
		dealerid: "maz-sfv-1",
		makeid: "maz",
		make: "Mazda",
		dealer: "Galpin Mazda",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",	
		phone: "3109290492",
		image: "mazda.jpg"
	},
	{
		dealerid: "maz-gld-1",
		makeid: "maz",
		make: "Mazda",
		dealer: "Star Mazda",
		area: "GLD",
		address: "1401 South Brand Boulevard Glendale 91204",
		phone: "3109290492",
		image: "mazda.jpg"
	},
	{
		dealerid: "maz-sgv-1",
		makeid: "maz",
		make: "Mazda",
		dealer: "Sierra Mazda",
		area: "SGV",
		address: "735 E. Central Ave Monrovia 91016",	
		phone: "3109290492",
		image: "mazda.jpg"
	},
	{
		dealerid: "maz-sby-1",
		makeid: "maz",
		make: "Mazda",
		dealer: "Browning Mazda",
		area: "SBY",
		address: "18827 Studebaker Road Cerritos 90703",	
		phone: "3109290492",
		image: "mazda.jpg"
	},
	{
		dealerid: "mer-sfv-1",
		makeid: "mer",
		make: "Mercury",
		dealer: "Vista Lincoln Mercury Motor Co",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",	
		phone: "3109290492",
		image: "mercury.jpg"
	},
	{
		dealerid: "mer-sfv-2",
		makeid: "mer",
		make: "Mercury",
		dealer: "Mayberry Lincoln Mercury",
		area: "SFV",
		address: "4437 Lankershim Blvd North Hollywood 91602",	
		phone: "3109290492",
		image: "mercury.jpg"
	},
	{
		dealerid: "mer-sfv-3",
		makeid: "mer",
		make: "Mercury",
		dealer: "Galpin Lincoln Mercury",
		area: "SFV",
		address: "15505 Roscoe Blvd North Hills 91343",
		phone: "3109290492",
		image: "mercury.jpg"
	},
	{
		dealerid: "mbz-sfv-1",
		makeid: "mbz",
		make: "Mercedes Benz",
		dealer: "Keyes European Mercedes Benz",
		area: "SFV",
		address: "5400 Van Nuys Boulevard Van Nuys 91401",	
		phone: "3109290492",
		image: "mercedes.jpg"
	},
	{
		dealerid: "mbz-sgv-1",
		makeid: "mbz",
		make: "Mercedes Benz",
		dealer: "Rusnak Arcadia Mercedes Benz",
		area: "SFV",
		address: "55 West Huntington Drive Arcadia 91007",	
		phone: "3109290492",
		image: "mercedes.jpg"
	},
	{
		dealerid: "mbz-mid-1",
		makeid: "mbz",
		make: "Mercedes Benz",
		dealer: "Downtown LA Motors Mercedes Benz",
		area: "MID",
		address: "1801 S. Figueroa St. Los Angeles 90015",	
		phone: "3109290492",
		image: "mercedes.jpg"
	},
	{
		dealerid: "mbz-mid-2",
		makeid: "mbz",
		make: "Mercedes Benz",
		dealer: "Mercedes Benz of Beverly Hills",
		area: "MID",
		address: "9250 Beverly Blvd Beverly Hills 90210",	
		phone: "3109290492",
		image: "mercedes.jpg"
	},
	{
		dealerid: "mbz-sby-1",
		makeid: "mbz",
		make: "Mercedes Benz",
		dealer: "Mercedes Benz of South Bay",
		area: "SBY",
		address: "3233 Pacific Coast Hwy Torrance 90505",	
		phone: "3109290492",
		image: "mercedes.jpg"
	},
	{
		dealerid: "nis-scv-1",
		makeid: "nis",
		make: "Nissan",
		dealer: "Nissan of Valencia",
		area: "SCV",
		address: "24111 Creekside Road Valencia, CA 91335",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "nis-sfv-1",
		makeid: "nis",
		make: "Nissan",
		dealer: "Nissan of Valencia",
		area: "SFV",
		address: "20539 Ventura Blvd Woodland Hills 91364",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "nis-sfv-2",
		makeid: "nis",
		make: "Nissan",
		dealer: "Miller Nissan",
		area: "SFV",
		address: "5425 Van Nuys Blvd Van Nuys 91401",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "nis-sby-1",
		makeid: "nis",
		make: "Nissan",
		dealer: "Power Nissan of Torrance",
		area: "SBY",
		address: "14610 Hindry Avenue Hawthorne 90250",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "nis-sby-2",
		makeid: "nis",
		make: "Nissan",
		dealer: "Power Nissan of Southbay",
		area: "SBY",
		address: "14610 Hindry Avenue Hawthorne 90250",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "nis-av-1",
		makeid: "nis",
		make: "Nissan",
		dealer: "Antelope Valley Nissan",
		area: "AV",
		address: "451 Auto Center Dr Palmdale 93551",	
		phone: "3109290492",
		image: "nissan.jpg"
	},
	{
		dealerid: "por-mid-1",
		makeid: "por",
		make: "Porsche",
		dealer: "Downtown LA Motors Porsche/Audi/Volkswagon",
		area: "MID",
		address: "1900 S. Figueroa St. Los Angeles 90007",	
		phone: "3109290492",
		image: "porsche.jpg"
	},
	{
		dealerid: "por-sby-1",
		makeid: "por",
		make: "Porsche",
		dealer: "Pacific Porsche",
		area: "SBY",
		address: "2900 Pacific Coast Hwy Torrance 90505",
		phone: "3109290492",
		image: "porsche.jpg"
	},
	{
		dealerid: "rol-mid-1",
		makeid: "rol",
		make: "Rolls Royce",
		dealer: "O'Gara Coach Company",
		area: "SBY",
		address: "8833 West Olympic Blvd Beverly Hills,CA,90211",	
		phone: "3109290492",
		image: "rollsroyce.jpg"
	},
	{
		dealerid: "sci-sgv-1",
		makeid: "sci",
		make: "Scion",
		dealer: "Wondries Toyota/Scion",
		area: "SGV",
		address: "1543 West Main Street Alhambra 91801",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-mid-1",
		makeid: "sci",
		make: "Scion",
		dealer: "Toyota/Scion Central Automotive",
		area: "MID",
		address: "1600 S Figueroa St Los Angeles Los Angeles 90015",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-wst-1",
		makeid: "sci",
		make: "Scion",
		dealer: "Marina del Rey Toyota Scion",
		area: "WST",
		address: "4636 Lincoln Blvd. Marina del Rey 90292",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-wst-2",
		makeid: "sci",
		make: "Scion",
		dealer: "Miller Toyota Scion Central Automotive",
		area: "WST",
		address: "9077 Washington Blvd. Culver City 90232",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-sby-1",
		makeid: "sci",
		make: "Scion",
		dealer: "Southbay Toyota Scion",
		area: "SBY",
		address: "18416 S Western Ave Gardena 90248",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-sby-2",
		makeid: "sci",
		make: "Scion",
		dealer: "Cabe Brothers Toyota Scion",
		area: "SBY",
		address: "2895 Long Beach Blvd Long Beach 90806",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sci-sby-3",
		makeid: "sci",
		make: "Scion",
		dealer: "Power Toyota Scion of Cerritos",
		area: "SBY",
		address: "8700 Studebaker Rd Cerritos 90703",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "sub-sfv-1",
		makeid: "sub",
		make: "Subaru",
		dealer: "Galpin Subaru",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",
		phone: "3109290492",
		image: "subaru.jpg"
	},
	{
		dealerid: "sub-sgv-1",
		makeid: "sub",
		make: "Subaru",
		dealer: "Sierra Subaru",
		area: "SGV",
		address: "731 E. Central Ave Monrovia 91016",
		phone: "3109290492",
		image: "subaru.jpg"
	},
	{
		dealerid: "toy-sfv-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Northridge Toyota",
		area: "SFV",
		address: "19550 Nordhoff Street Northridge 91324",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sfv-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "Keyes Toyota",
		area: "SFV",
		address: "5855 Van Nuys Blvd Van Nuys 91401",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-gld-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Bob Smith Toyota",
		area: "GLD",
		address: "3333 Foothill Blvd La Crescenta-Montrose 91214",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-gld-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "Toyota of Glendale",
		area: "GLD",
		address: "1260 South Brand Boulevard Glendale 91204",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sgv-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Toyota of Pasadena",
		area: "SGV",
		address: "3600 East Foothill Boulevard Pasadena 91107",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sgv-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "West Covina Toyota",
		area: "SGV",
		address: "1800 E Garvey Ave S West Covina 91791",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sgv-3",
		makeid: "toy",
		make: "Toyota",
		dealer: "Puenta Hills Toyota",
		area: "SGV",
		address: "17070 Gale Ave City of Industry 91748",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sgv-4",
		makeid: "toy",
		make: "Toyota",
		dealer: "Wondries Toyota/Scion",
		area: "SGV",
		address: "1543 West Main Street Alhambra 91801",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-mid-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Toyota of Hollywood",
		area: "MID",
		address: "6000 Hollywood Boulevard Hollywood 90028",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-mid-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "Toyota/Scion Central Automotive",
		area: "MID",
		address: "1600 S Figueroa St Los Angeles Los Angeles 90015",
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-wst-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Marina del Rey Toyota Scion",
		area: "WST",
		address: "4636 Lincoln Blvd. Marina del Rey 90292",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-wst-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "Toyota of Santa Monica",
		area: "WST",
		address: "801 Santa Monica Boulevard Santa Monica 90404",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-wst-3",
		makeid: "toy",
		make: "Toyota",
		dealer: "Miller Toyota Scion Central Automotive",
		area: "WST",
		address: "9077 Washington Blvd. Culver City 90232",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sby-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Longo Toyota/Lexus",
		area: "SBY",
		address: "3530 North Peck Road El Monte 91731",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sby-2",
		makeid: "toy",
		make: "Toyota",
		dealer: "Southbay Toyota Scion",
		area: "SBY",
		address: "18416 S Western Ave Gardena 90248",	
		phone: "3109290492",
		image: "scion.jpg"
	},
	{
		dealerid: "toy-sby-3",
		makeid: "toy",
		make: "Toyota",
		dealer: "Cabe Brothers Toyota Scion",
		area: "SBY",
		address: "2895 Long Beach Blvd Long Beach 90806",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-sby-4",
		makeid: "toy",
		make: "Toyota",
		dealer: "Power Toyota Scion of Cerritos",
		area: "SBY",
		address: "8700 Studebaker Rd Cerritos 90703",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "toy-av-1",
		makeid: "toy",
		make: "Toyota",
		dealer: "Sierra Toyota Lancaster",
		area: "AV",
		address: "43301 12th St. W Lancaster 93534",	
		phone: "3109290492",
		image: "toyota.jpg"
	},
	{
		dealerid: "vol-sfv-1",
		makeid: "vol",
		make: "Volvo",
		dealer: "Galpin Volvo",
		area: "SFV",
		address: "21701 Ventura Boulevard Woodland Hills 91364",
		phone: "3109290492",
		image: "volvo.jpg"
	},
	{
		dealerid: "vol-sfv-2",
		makeid: "vol",
		make: "Volvo",
		dealer: "Volvo of Calabasas",
		area: "SFV",
		address: "24400 Calabasas Road Calabasas 91302",
		phone: "3109290492",
		image: "volvo.jpg"
	},
	{
		dealerid: "vol-sgv-1",
		makeid: "vol",
		make: "Volvo",
		dealer: "Rusnak Volvo",
		area: "SGV",
		address: "2025 East Colorado Boulevard Pasadena 91007",	
		phone: "3109290492",
		image: "volvo.jpg"
	},
	{
		dealerid: "vol-sby-1",
		makeid: "vol",
		make: "Volvo",
		dealer: "Power Volvo Southbay",
		area: "SBY",
		address: "3010 Pacific Coast Hwy Torrance 90505",	
		phone: "3109290492",
		image: "volvo.jpg"
	},
	{
		dealerid: "vlk-sfv-1",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "Livingston Volkswagen",
		area: "SFV",
		address: "21141 Ventura Boulevard Woodland Hills 91364",
		phone: "3109290492",
		image: "vw.jpg"
	},
	{
		dealerid: "vlk-sfv-2",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "Volkswagen of Van Nuys",
		area: "SFV",
		address: "6115 Van Nuys Blvd Van Nuys 91401",	
		phone: "3109290492",
		image: "vw.jpg"
	},
	{
		dealerid: "vlk-gld-1",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "New Century Volkswagen",
		area: "GLD",
		address: "1220 South Brand Boulevard Glendale 91204",	
		phone: "3109290492",
		image: "vw.jpg"
	},
	{
		dealerid: "vlk-sgv-1",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "Volkswagen Pasadena",
		area: "SGV",
		address: "130 North Sierra Madre Boulevard Pasadena 91107",	
		phone: "3109290492",
		image: "vw.jpg"
	},
	{
		dealerid: "vlk-wst-1",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "Volkswagen Santa Monica",
		area: "WST",
		address: "2440 Santa Monica Boulevard Santa Monica 90404",	
		phone: "3109290492",
		image: "vw.jpg"
	},
	{
		dealerid: "vlk-sby-1",
		makeid: "vlk",
		make: "Volkswagen",
		dealer: "Pacific Volkswagen",
		area: "SBY",
		address: "14900 Hindry Avenue Hawthorne 90250",	
		phone: "3109290492",
		image: "vw.jpg"
	},

];
