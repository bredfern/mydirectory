$(document).bind( "pagebeforechange", function( e, data ) {
	if ( typeof data.toPage === "string" ) {
		var u = $.mobile.path.parseUrl( data.toPage ), 
		reM = /^#models/, 
		reD = /^#dealer/,
		reH = /^#makes/;
		if ( u.hash.search(reM) !== -1 ) {
			modelsMenuDisplay( u, data.options );
			e.preventDefault();
		} else if ( u.hash.search(reD) !== -1) {
			dealerInfoDisplay( u, data.options );
			e.preventDefault();
		} else if (u.hash.search(reH) !== -1) {
			makeMenuDisplay( u, data.options );
			e.preventDefault();
		}
	}
});

var makeMenuDisplay = function( urlObj, options ) {
	var pageSelector = urlObj.hash.replace( /\?.*$/, "" ),
	lastMake,
	mItems = dealerData;
	
	var $page = $( pageSelector ),
	$page = $( pageSelector ),
	$header = $page.children( ":jqmData(role=header)" ),
	$content = $page.children( ":jqmData(role=content)" ),
	numItems = mItems.length, 
	markup = '<ul data-role="listview" data-inset="true"><li data-role="list-divider" role="heading">Choose a Dealer</li>';
	for (var i = 0; i < numItems; i++) {
		if (mItems[i].make !== lastMake) {
			markup += '<li><a data-transition="none" href="#models?category=' + mItems[i].makeid + '">' + mItems[i].make + '</a></li>';
		}
		lastMake = mItems[i].make;
	}
	markup += "</ul>";
	//console.log(markup);
	$content.html( markup );
	$page.page();
	$content.find( ":jqmData(role=listview)" ).listview();
	options.dataUrl = urlObj.href;
	$.mobile.changePage( $page, options );
}

var modelsMenuDisplay = function( urlObj, options )
{
	var modelName = urlObj.hash.replace( /.*category=/, "" ), model = dealerData[ modelName ];
	var pageSelector = urlObj.hash.replace( /\?.*$/, "" );
	
	var $page = $( pageSelector ),
		$header = $page.children( ":jqmData(role=header)" ),
		$content = $page.children( ":jqmData(role=content)" ),
		markup = '<ul data-role="listview" data-inset="true"><li data-role="list-divider" role="heading">Choose a Dealer</li>',
		mItems = dealerData,
		numItems = mItems.length;

	for (var i = 0; i < numItems; i++ ) {
		if (mItems[i].makeid == modelName) {
			markup += "<li><a data-transition='none' href='#dealer?dealerid=" + mItems[i].dealerid + "'>" + mItems[i].dealer + "</a></li>";
		} 
	}
		
	markup += "</ul>";
	$content.html( markup );
	$page.page();
	$content.find( ":jqmData(role=listview)" ).listview();
	options.dataUrl = urlObj.href;
	$.mobile.changePage( $page, options );
}

var dealerInfoDisplay = function( urlObj, options )
{
	var pageSelector = urlObj.hash.replace( /\?.*$/, "" );
	var dealerID = urlObj.hash.replace(/.*dealerid=/, "");
	
	var $page = $( pageSelector ),
		$header = $page.children( ":jqmData(role=header)" ),
		$content = $page.children( ":jqmData(role=content)" ),
		dItems = dealerData,
		numItems = dItems.length,
		markup = "<div class='ui-grid-a'>";
		
	for ( var i = 0; i < numItems; i++ ) {
		console.log(dealerID);
		if (dItems[i].dealerid === dealerID) {
			markup += "<div class='ui-block-a'><a data-transition='none' href='#models?category=" + dItems[i].makeid + "' data-transition='fade'><img src='images/cars/" + dItems[i].image + "' class='auto-image' /><br /><strong>More " + dItems[i].make + " Dealers</strong></a></div>";
			markup += "<div class='ui-block-b'>";
			markup += "<h3 class='dealer-name'>" + dItems[i].dealer + "</h3>";
			markup += dItems[i].address + "<br />";
			markup += "<a data-role='button' href='mailto:mobile@autola.com?subject=Sales%20Lead&amp;body=I%20would%20like%20" + dItems[i].dealer + "%20to%20contact%20me'>Contact</a>";
			markup += "</div>";
		}
	}
	
	markup += "</div>";
	//console.log(markup);
	$content.html( markup );
	$page.page();
	options.dataUrl = urlObj.href;
	$.mobile.changePage($page, options);
	$page.trigger('pagecreate');
}

function onBackKeyDown() {
		var ismobile = (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
		var istablet = (/ipad|android|android 3.0|xoom|sch-i800|playbook|tablet|kindle/i.test(navigator.userAgent.toLowerCase()));
		if (istablet) {
			$.mobile.changePage( "#home" );
		}
}

function onDeviceReady () {
//add the device ready listener
	document.addEventListener("backbutton", onBackKeyDown, false);
}
document.addEventListener("deviceready", onDeviceReady, false);

